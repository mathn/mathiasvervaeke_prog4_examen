<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = ['name','code','longitude','latitude','shippingcostmultiplier'];
    protected $table = 'countries';
    
    public function customers(){
        return $this->hasMany('App\Customer','idcountry','id');
    }
}
