<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customers';
    protected $fillable = ['nickname','firstname','lastname','address1','address2','city','postalcode','region','phone','mobile','idcountry'];
    public function countries(){
        return $this->belongsTo('App\Country','idcountry','id');
    }
}
