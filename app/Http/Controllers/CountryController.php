<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CountryRequest;
use App\Country;
use App\Customer;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countries = Country::all();
        
        return view('country.index', ['countries' => $countries]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::all();
        return view('country.create')->withCountries($countries);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CountryRequest $request)
    {
        Country::Create($request->all());
        return redirect()->route('country.index')->with('message','The new country has been successfully added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $countries = Country::all();
        $country = Country::find($id);
        //$countries = Country::find($customer->idcountry);
        
        return view('country.show',array(  'countries'=>$countries,
                                            'country'=>$country));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $countries = Country::all();
        $country = Country::find($id);
        //$countries = Country::find($customer->idcountry);
        
        return view('country.edit',array(  
                                            'country'=>$country,'countries'=>$countries));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CountryRequest $request, Country $country)
    {
        $country->update($request->all());
        return redirect()->route('country.index')->with('message','The country has been succssfully edited.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = Customer::where('idcountry' , '=', $id)->first();
        if($customer != null){
            return redirect()->route('country.index')->with('error','There are existing customers with this country!');
        };
        if($customer == null){
            $country = Country::findOrFail($id);
        $country->delete();
        return redirect()->route('country.index')->with('message','The country has been successfully deleted.');
        };
        
    }
}
