<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Customer;
use App\Http\Requests\CustomerRequest;
use App\Country;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::all();
        
        return view('customer.index', ['customers' => $customers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$countries = Country::pluck('name', 'id');
        
        $countryfail = Country::first();
        if($countryfail == null){
            return redirect()->route('customer.index')->with('error','Please add a country first!'); 
        }
        elseif($countryfail != null){
            $countries = Country::all();
            $customers = Customer::all();
            return view('customer.create')->withCountries($countries)->withCustomers($customers); 
        };
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CustomerRequest $request)
    {
        Customer::Create($request->all());
        return redirect()->route('customer.index')->with('message','The new customer has been successfully added.');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customers = Customer::all();
        $customer = Customer::find($id);
        //$countries = Country::find($customer->idcountry);
        
        return view('customer.show',array(  'customers'=>$customers,
                                            'customer'=>$customer));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $countries = Country::pluck('name', 'id');
        //$countries = Country::table('countries')->lists('name', 'id','code');
        $customers = Customer::all();
        $customer = Customer::find($id);
        //$countries = Country::find($customer->idcountry);
        
        return view('customer.edit',array(  'customers'=>$customers,
                                            'customer'=>$customer,'countries'=>$countries));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CustomerRequest $request, Customer $customer)
    {
        $customer->update($request->all());
        return redirect()->route('customer.index')->with('message','The customer has been succssfully edited.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = Customer::findOrFail($id);
        $customer->delete();
        return redirect()->route('customer.index')->with('message','The customer has been successfully deleted.');
    }
}
