<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Product;
use App\Http\Requests\ProductRequest;
use App\Category;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        
        return view('product.index', ['products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$categories = category::pluck('name', 'id');
        
        
        $categoryfail = Category::first();
        if($categoryfail == null){
            return redirect()->route('product.index')->with('error','Please add a category first!'); 
        }
        elseif($categoryfail != null){
            $categories = Category::all();
            $products = Product::all();
            return view('product.create')->withCategories($categories)->withProducts($products);
        };
        
        
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        
        Product::Create($request->all());
        return redirect()->route('product.index')->with('message','The new product has been successfully added.');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $products = Product::all();
        $product = Product::find($id);
        //$categories = category::find($product->idcategory);
        
        return view('product.show',array(  'products'=>$products,
                                            'product'=>$product));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::pluck('name', 'id');
        //$categories = category::table('categories')->lists('name', 'id','code');
        $products = Product::all();
        $product = Product::find($id);
        //$categories = category::find($product->idcategory);
        
        return view('product.edit',array(  'products'=>$products,
                                            'product'=>$product,'categories'=>$categories));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, product $product)
    {
        $product->update($request->all());
        return redirect()->route('product.index')->with('message','The product has been succssfully edited.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();
        return redirect()->route('product.index')->with('message','The product has been successfully deleted.');
    }
}

