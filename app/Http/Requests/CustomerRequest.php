<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nickname'=> 'required|max:10',
            'firstname'=>'required|max:255',
            'lastname'=>'required|max:255',
            'address1'=>'required|max:255',
            'address2'=>'max:255',
            'city'=>'required|max:255',
            'region'=>'max:80',
            'postalcode'=>'required|max:20',
            'idcountry'=>'required|min:0',
            'phone'=>'max:40',
            'mobile'=>'max:40'
        ];
    }
}
