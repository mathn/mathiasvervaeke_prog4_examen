<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description'=>'max:1024',
            'name'=>'required|max:255',
            'price'=>'numeric',
            'shippingcost'=>'numeric',
            'totalrating'=>'numeric|min:0',
            'thumbnail'=>'max:255',
            'image'=>'max:255',
            'discountpercentage'=>'numeric',
            'votes'=>'numeric|min:0',
            'idcategory'=>'numeric|min:0'
        ];
    }
}
