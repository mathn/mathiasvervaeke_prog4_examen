<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $fillable = ['name','description','price','discountpercentage','shippingcost','votes','totalrating','idcategory'];
    public function categories(){
        return $this->belongsTo('App\Category','idcategory','id');
    }
}
