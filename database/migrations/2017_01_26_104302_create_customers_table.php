<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) 
        {
            $table->increments('id');
            $table->string('nickname',10);
            $table->string('firstname',255);
            $table->string('lastname',255);
            $table->string('address1',255);
            $table->string('address2',255)->nullable();
            $table->string('city',255);
            $table->string('postalcode',20);
            $table->string('region',80)->nullable();
            $table->string('phone',40)->nullable();
            $table->string('mobile',40)->nullable();
            $table->integer('idcountry')->unsigned();
            $table->foreign('idcountry')->references('id')->on('countries');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}