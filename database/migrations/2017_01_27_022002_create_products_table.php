<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products',function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->string('description')->nullable();
            $table->float('price')->nullable();
            $table->float('discountpercentage')->nullable();
            $table->float('shippingcost')->nullable();
            $table->binary('thumbnail')->nullable();
            $table->binary('image')->nullable();
            $table->integer('votes')->nullable();
            $table->integer('totalrating')->nullable();
            $table->integer('idcategory')->unsigned();
            $table->foreign('idcategory')->references('id')->on('categories');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}