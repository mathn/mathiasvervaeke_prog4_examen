@extends('layouts.adminpage') 
<style>
    *{
        box-sizing: border-box;
            -moz-box-sizing: border-box;
            margin: 0;
            padding: 0;
            border: 0;
    }
        html, body{
            
            height:100%;
            width:100%;
        }
        
        .tower{
            height:100%;
            width:100%;
            background-color: blue;
        }
        
        .floor{
            height:100%;
            width:100%;
            background-color: yellow;
        }

        .show-room{
            height:80%;
            width:90%;
            background-color: green;
            margin-left: 5%;
            margin-bottom: 5%;
        }
        
        .tile{
            height:33.33%;
            width:25%;
            background-color: #337AB7;
            border-left: solid white 0.3em;
            border-top: solid white 0.3em;
            border-bottom: solid white 0.3em;
            border-right: solid white 0.3em;
            float: left;
            text-align: center;
            vertical-align: middle;
            color: white;
        }
        .largetile{
            height:33.33%;
            width:50%;
            background-color: #337AB7;
            border-left: solid white 0.3em;
            border-top: solid white 0.3em;
            border-bottom: solid white 0.3em;
            border-right: solid white 0.3em;
            float: left;
            text-align: center;
            vertical-align: middle;
            color: white;
        }
        .mask{
            text-align: center;
            vertical-align: middle;
        }
        .e:hover{
               background-color:grey;
               color:#337AB7;
        }
    </style>
@section('content')
<div class="container">
            <article class="show-room">
                <a href="/product" class="mask">
                <div class="tile e">
                    
                        <h2 class="mask">Product</h2>
                    
                </div>
                </a>
                <a href="" class="mask">
                <div class="tile e">
                    
                        <h2 class="mask">Supplier</h2>
                    
                </div>
                </a>
                <a href="" class="mask">
                <div class="largetile">
                    
                        <img src='{{URL::to("images/foto3.jpg")}}' style="height:100%;width:100%;" />
                    
                </div>
                </a>
                <a href="/customer" class="mask">
                <div class="tile e">
                    
                        <h2 class="mask">Customer</h2>
                    
                </div>
                </a>
                <a href="" class="mask">
                <div class="tile">
                    
                        <img src='{{URL::to("images/foto1.jpg")}}' style="height:100%;width:100%;" />
                    
                </div>
                </a>
                <a href="" class="mask">
                <div class="tile e">
                    
                        <h2 class="mask">Order</h2>
                    
                </div>
                </a>
                <a href="" class="mask">
                <div class="tile e">
                    
                        <h2 class="mask">Order Items</h2>
                    
                </div>
                </a>
                <a href="" class="mask">
                <div class="tile e">
                    
                        <img src='{{URL::to("images/foto2.jpg")}}' style="height:100%;width:100%;" />
                    
                </div>
                </a>
                <a href="/country" class="mask">
                <div class="tile e">
                    
                        <h2 class="mask">Country</h2>
                    
                </div>
                </a>
                <a href="" class="mask">
                <div class="tile e">
                    
                        <h2 class="mask">Order Status</h2>
                    
                </div>
                </a>
                 <a href="/category" class="mask">
                <div class="tile e">
                    
                        <h2 class="mask">Category</h2>
                    
                </div>
                </a>
            </article>
</div>
@endsection