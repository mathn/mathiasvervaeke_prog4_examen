@extends('layouts.app') @section('content')
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col pull-left" style="padding:5px;border:2px">
                            <font size="5">Category</font>
                        </div>
                        <div class="col pull-right" style="padding: 5px;border:2px">
                            {!! Form::open(array('url'=>'category.store')) !!}
                            {!!Form::button('Insert',['type'=>'submit','class'=>'btn btn-default'])!!}
                            {{ link_to_route('category.index','Cancel',null,['class'=>'btn btn-default']) }}
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="form-group">
                        {!!Form::label('name','Naam')!!}
                        {!!Form::text('name',null,['class'=>'form-control'])!!}
                    </div>

                    <div class="form-group">
                        {!!Form::label('description','Beschrijving')!!}
                        {!!Form::text('description',null,['class'=>'form-control'])!!}
                    </div>

                    {!! Form::close() !!}
                    
                </div>
            </div>
            @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
            @endif
        </div>
        <div class="row">
            <div class="col-md-3 col-md-6 sidebar">
                <table class="table table-striped list-group">
                            <tr>
                                <td>Select</td>
                                <td>Naam</td>
                                <td>Beschrijving</td>
                            </tr>
                            @foreach($categories as $item)
                            <tr>
                                <td>
                                      <a href="{{ URL::route('category.show',[$item->id]) }}" class="list-group-item">></a>
                                </td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->description}}</td>
                            </tr>
                            @endforeach
                        </table>
            </div>
        </div>
    </div>
</div>
@endsection