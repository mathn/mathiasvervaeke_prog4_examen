@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                        <div class="col pull-left" style="padding:5px;border:2px">
                            <font size="5">Category</font>
                        </div>
                        <div class="col pull-right" style="padding: 5px;border:2px">
                            {!! Form::model($category,array('route'=>['category.update',$category],'method'=>'PUT')) !!}
                            {!! Form::button('Update',['type'=>'submit','class'=>'btn btn-primary']) !!}
                            {{ link_to_route('category.index','Cancel',null,['class'=>'btn btn-default']) }}
                        </div>
                    </div>
                    </div>

                    <div class="panel-body">

                        {!! Form::model($category,array('route'=>['category.update',$category],'method'=>'PUT')) !!}
                    <div class="form-group">
                        {!!Form::label('name','Naam')!!}
                        {!!Form::text('name',null,['class'=>'form-control'])!!}
                    </div>
                    <div class="form-group">
                        {!!Form::label('description','Naam')!!}
                        {!!Form::text('description',null,['class'=>'form-control'])!!}
                    </div>

                    <div>
                        {!! Form::close() !!}

                    </div>
                </div>

                

            </div>
            @if($errors->any())
                    <ul class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
        </div>
        <div class="row">
            <div class="col-md-3 col-md-6 sidebar">
                <table class="table table-striped list-group">
                            <tr>
                                <td>Select</td>
                                <td>Naam</td>
                                <td>Beschrijving</td>
                            </tr>
                            @foreach($categories as $item)
                            @if($category->id == $item->id)
                            <tr>
                                <td>
                                      <a href="{{ URL::route('category.show',[$item->id]) }}" class="list-group-item active">></a>
                                </td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->description}}</td>
                            </tr>
                            @elseif($category->id != $item->id)
                            <tr>
                                <td>
                                      <a href="{{ URL::route('category.show',[$item->id]) }}" class="list-group-item">></a>
                                </td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->description}}</td>
                            </tr>
                            @endif
                            @endforeach
                        </table>
            </div>
        </div>
    </div>
@endsection