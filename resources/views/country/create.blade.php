@extends('layouts.app') @section('content')
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                     <div class="row">
                        <div class="col pull-left" style="padding:5px;border:2px">
                            <font size="5">Country</font>
                        </div>
                        <div class="col pull-right" style="padding: 5px;border:2px">
                            {!! Form::open(array('url'=>'country.store')) !!}
                            {!!Form::button('Insert',['type'=>'submit','class'=>'btn btn-default'])!!}
                            {{ link_to_route('country.index','Cancel',null,['class'=>'btn btn-default']) }}
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    {!! Form::open(array('url'=>'country.store')) !!}
                    <div class="form-group">
                        {!!Form::label('name','Naam')!!}
                        {!!Form::text('name',null,['class'=>'form-control'])!!}
                    </div>

                    <div class="form-group">
                        {!!Form::label('code','Code')!!}
                        {!!Form::text('code',null,['class'=>'form-control'])!!}
                    </div>
                    
                    <div class="form-group">
                        {!!Form::label('longitude','Lengtegraad')!!}
                        {!!Form::text('longitude',null,['class'=>'form-control'])!!}
                    </div>
                    
                    <div class="form-group">
                        {!!Form::label('latitude','Breedtegraad')!!}
                        {!!Form::text('latitude',null,['class'=>'form-control'])!!}
                    </div>
                    
                    <div class="form-group">
                        {!!Form::label('shippingcostmultiplier','Verzendkosten')!!}
                        {!!Form::text('shippingcostmultiplier',null,['class'=>'form-control'])!!}
                    </div>

                    {!! Form::close() !!}
                    
                </div>
            </div>
            @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
            @endif
        </div>
        <div class="row">
            <div class="col-md-3 col-md-6 sidebar">
                <table class="table table-striped list-group">
                            <tr>
                                <td>Select</td>
                                <td>Code</td>
                                <td>Naam</td>
                            </tr>
                            @foreach($countries as $item)
                            <tr>
                                <td>
                                      <a href="{{ URL::route('country.show',[$item->id]) }}" class="list-group-item">></a>
                                </td>
                                <td>{{$item->code}}</td>
                                <td>{{$item->name}}</td>
                            </tr>
                            @endforeach
                        </table>
            </div>
        </div>
    </div>
</div>
@endsection