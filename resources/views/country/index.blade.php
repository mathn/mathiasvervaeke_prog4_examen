@extends('layouts.app') @section('content')
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col pull-left" style="padding:5px;border:2px">
                            <font size="5">Country</font>
                        </div>
                        <div class="col pull-right" style="padding: 5px;border:2px">
                            {{ link_to_route('country.create','Inserting',null,['class'=>'btn btn-default']) }}
                        </div>
                    </div>
                </div>
            </div>
           @if(Session::has('message'))
                <div class="alert alert-success">{{Session::get('message')}}</div>
            @elseif(Session::has('error'))
                <div class="alert alert-danger">{{Session::get('error')}}</div>
            @endif
        </div>
                     
          <div class="row">
            <div class="col-md-3 col-md-6 sidebar">
                <table class="table table-striped list-group">
                            <tr>
                                <td>Select</td>
                                <td>Code</td>
                                <td>Naam</td>
                            </tr>
                            @foreach($countries as $item)
                            <tr>
                                <td>
                                      <a href="{{ URL::route('country.show',[$item->id]) }}" class="list-group-item">></a>
                                </td>
                                <td>{{$item->code}}</td>
                                <td>{{$item->name}}</td>
                            </tr>
                            @endforeach
                        </table>
            </div>
        </div>
    </div>
    
   
</div>
@endsection