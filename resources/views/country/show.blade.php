@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading"><div class="row">
                        <div class="col pull-left" style="padding:5px;border:2px">
                            <font size="5">Country</font>
                        </div>
                        <div class="col pull-right" style="padding: 5px;border:2px">
                            {!! Form::open(['method' => 'DELETE','route' => ['country.destroy', $country->id]]) !!}
                            {{ link_to_route('country.edit','Updating',[$country->id],['class'=>'btn btn-primary']) }} 
                            {{ link_to_route('country.create','Inserting',null,['class'=>'btn btn-primary']) }}
                            {!! Form::submit('Delete', ['class' => 'btn btn-primary']) !!}
                            {{ link_to_route('country.index','Cancel',null,['class'=>'btn btn-default']) }}
                        </div>
                    </div>
                        
                    </div>

                    <div class="panel-body">
                    <div class="form-group">
                        {!!Form::label('name','Naam')!!}<br> {!!Form::text('name',$country->name,['class'=>'form-control','readonly' => true])!!}
                    </div>
                    <div class="form-group">
                        {!!Form::label('code','Code')!!}<br> {!!Form::text('code',$country->code,['class'=>'form-control','readonly' => true])!!}
                    </div>
                    <div class="form-group">
                        {!!Form::label('latitude','Breedtegraad')!!}<br> {!!Form::text('latitude',$country->latitude,['class'=>'form-control','readonly' => true])!!}
                    </div>
                    <div class="form-group">
                        {!!Form::label('longitude','Lengtegraad')!!}<br> {!!Form::text('longitude',$country->longitude,['class'=>'form-control','readonly' => true])!!}
                    </div>
                    <div class="form-group">
                        {!!Form::label('shippingcostmultiplier','Verzendkosten')!!}<br> {!!Form::text('shippingcostmultiplier',$country->shippingcostmultiplier,['class'=>'form-control','readonly' => true])!!}
                    </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
            <div class="row">
            <div class="col-md-3 col-md-6 sidebar">
                <table class="table table-striped list-group">
                            <tr>
                                <td>Select</td>
                                <td>Code</td>
                                <td>Naam</td>
                            </tr>
                            @foreach($countries as $item)
                            @if($country->id == $item->id)
                            <tr>
                                <td>
                                      <a href="{{ URL::route('country.show',[$item->id]) }}" class="list-group-item active">></a>
                                </td>
                                <td>{{$item->code}}</td>
                                <td>{{$item->name}}</td>
                            </tr>
                            @elseif($country->id != $item->id)
                            <tr>
                                <td>
                                      <a href="{{ URL::route('country.show',[$item->id]) }}" class="list-group-item">></a>
                                </td>
                                <td>{{$item->code}}</td>
                                <td>{{$item->name}}</td>
                            </tr>
                            @endif
                            @endforeach
                        </table>
            </div>
        </div>
        </div>
        
    </div>
@endsection