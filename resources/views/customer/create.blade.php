@extends('layouts.app') @section('content')
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col pull-left" style="padding:5px;border:2px">
                            <font size="5">Customer</font>
                        </div>
                        <div class="col pull-right" style="padding: 5px;border:2px">
                            {!! Form::open(array('url'=>'customer.store')) !!}
                            {!!Form::button('Insert',['type'=>'submit','class'=>'btn btn-default'])!!}
                            {{ link_to_route('customer.index','Cancel',null,['class'=>'btn btn-default']) }}
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    
                    <div class="form-group">
                        {!!Form::label('nickname','Alias')!!}
                        {!!Form::text('nickname',null,['class'=>'form-control'])!!}
                    </div>
                    <div class="form-group">
                        {!!Form::label('firstname','Voornaam')!!}
                        {!!Form::text('firstname',null,['class'=>'form-control'])!!}
                    </div>

                    <div class="form-group">
                        {!!Form::label('lastname','Familienaam')!!}
                        {!!Form::text('lastname',null,['class'=>'form-control'])!!}
                    </div>
                    
                    <div class="form-group">
                        {!!Form::label('address1','Adres 1')!!}
                        {!!Form::text('address1',null,['class'=>'form-control'])!!}
                    </div>
                    
                    <div class="form-group">
                        {!!Form::label('address2','Adres 2')!!}
                        {!!Form::text('address2',null,['class'=>'form-control'])!!}
                    </div>
                    <div class="form-group">
                        {!!Form::label('city','Stad')!!}
                        {!!Form::text('city',null,['class'=>'form-control'])!!}
                    </div>
                    <div class="form-group">
                        {!!Form::label('postalcode','Postcode')!!}
                        {!!Form::text('postalcode',null,['class'=>'form-control'])!!}
                    </div>
                    <div class="form-group">
                        {!!Form::label('region','regio')!!}
                        {!!Form::text('region',null,['class'=>'form-control'])!!}
                    </div>
                     <div class="form-group">
                        {!!Form::label('phone','Telefoon')!!}
                        {!!Form::text('phone',null,['class'=>'form-control'])!!}
                    </div>
                     <div class="form-group">
                        {!!Form::label('mobile','Mobiel')!!}
                        {!!Form::text('mobile',null,['class'=>'form-control'])!!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('idcountry','Land') !!}
                        <select class="form-control" name="idcountry">
                            @foreach($countries as $country)
                            <option value="{{$country->id}}">{{$country->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        
                        
                    </div>
                    {!! Form::close() !!}
                    
                </div>
            </div>
            @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
            @endif
            
        </div>
        <div class="row">
            <div class="col-md-3 col-md-6 sidebar">
                <table class="table table-striped list-group">
                            <tr>
                                <td>Select</td>
                                <td>Alias</td>
                                <td>Voornaam</td>
                            </tr>
                            @foreach($customers as $item)
                            <tr>
                                <td>
                                      <a href="{{ URL::route('customer.show',[$item->id]) }}" class="list-group-item">></a>
                                </td>
                                <td>{{$item->nickname}}</td>
                                <td>{{$item->firstname}}</td>
                            </tr>
                            @endforeach
                        </table>
            </div>
        </div>
    </div>
</div>
@endsection