@extends('layouts.app') @section('content')
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col pull-left" style="padding:5px;border:2px">
                            <font size="5">Customer</font>
                        </div>
                        <div class="col pull-right" style="padding: 5px;border:2px">
                            {!! Form::open(['method' => 'DELETE','route' => ['customer.destroy', $customer->id]]) !!}
                            {{ link_to_route('customer.edit','Updating',[$customer->id],['class'=>'btn btn-primary']) }} 
                            {{ link_to_route('customer.create','Inserting',null,['class'=>'btn btn-primary']) }}
                            {!! Form::submit('Delete', ['class' => 'btn btn-primary']) !!}
                            {{ link_to_route('customer.index','Cancel',null,['class'=>'btn btn-default']) }}
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="form-group">
                        {!!Form::label('nickname','Roepnaam')!!}<br> {!!Form::text('nickname',$customer->nickname,['class'=>'form-control','readonly' => true])!!}
                    </div>
                    <div class="form-group">
                        {!!Form::label('firstname','Voornaam')!!}<br> {!!Form::text('firstname',$customer->firstname,['class'=>'form-control','readonly' => true])!!}
                    </div>
                    <div class="form-group">
                        {!!Form::label('lastname','Familienaam')!!}<br> {!!Form::text('lastname',$customer->lastname,['class'=>'form-control','readonly' => true])!!}
                    </div>
                    <div class="form-group">
                        {!!Form::label('address1','Adres 1')!!}<br> {!!Form::text('address1',$customer->address1,['class'=>'form-control','readonly' => true])!!}
                    </div>
                    <div class="form-group">
                        {!!Form::label('address2','Adres 2')!!}<br> {!!Form::text('address2',$customer->address2,['class'=>'form-control','readonly' => true])!!}
                    </div>
                    <div class="form-group">
                        {!!Form::label('city','Stad')!!}<br> {!!Form::text('city',$customer->city,['class'=>'form-control','readonly' => true])!!}
                    </div>
                    <div class="form-group">
                        {!!Form::label('region','Regio')!!}<br> {!!Form::text('region',$customer->region,['class'=>'form-control','readonly' => true])!!}
                    </div>
                    <div class="form-group">
                        {!!Form::label('postalcode','Postcode')!!}<br> {!!Form::text('postalcode',$customer->postalcode,['class'=>'form-control','readonly' => true])!!}
                    </div>
                    <div class="form-group">
                        {!!Form::label('idcountry','Land')!!}<br> {!!Form::text('idcountry',$customer->countries->name,['class'=>'form-control','readonly' => true])!!}
                    </div>
                    <div class="form-group">
                        {!!Form::label('phone','Telefoon')!!}<br> {!!Form::text('phone',$customer->phone,['class'=>'form-control','readonly' => true])!!}
                    </div>
                    <div class="form-group">
                        {!!Form::label('mobile','Mobieltje')!!}<br> {!!Form::text('mobile',$customer->mobile,['class'=>'form-control','readonly' => true])!!}
                    </div>
                </div>
            </div>
        </div>
                            {!! Form::close() !!}

        <div class="row">
            <div class="col-md-3 col-md-6 sidebar">
                <table class="table table-striped list-group">
                            <tr>
                                <td>Select</td>
                                <td>Alias</td>
                                <td>Voornaam</td>
                            </tr>
                            @foreach($customers as $item)
                            @if($customer->id == $item->id)
                            <tr>
                                <td>
                                      <a href="{{ URL::route('customer.show',[$item->id]) }}" class="list-group-item active">></a>
                                </td>
                                <td>{{$item->nickname}}</td>
                                <td>{{$item->firstname}}</td>
                            </tr>
                            @elseif($customer->id != $item->id)
                            <tr>
                                <td>
                                      <a href="{{ URL::route('customer.show',[$item->id]) }}" class="list-group-item">></a>
                                </td>
                                <td>{{$item->nickname}}</td>
                                <td>{{$item->firstname}}</td>
                            </tr>
                            @endif
                            @endforeach
                        </table>
            </div>
        </div>
    </div>
</div>
@endsection