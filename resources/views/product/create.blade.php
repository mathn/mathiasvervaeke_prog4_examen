@extends('layouts.app') @section('content')
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col pull-left" style="padding:5px;border:2px">
                            <font size="5">Product</font>
                        </div>
                        <div class="col pull-right" style="padding: 5px;border:2px">
                            {!! Form::open(array('url'=>'product.store')) !!}
                            {!!Form::button('Insert',['type'=>'submit','class'=>'btn btn-default'])!!}
                            {{ link_to_route('product.index','Cancel',null,['class'=>'btn btn-default']) }}
                        </div>
                    </div>
                </div>

                <div class="panel-body">

                    <div class="form-group">
                        {!!Form::label('name','Naam')!!}
                        {!!Form::text('name',null,['class'=>'form-control'])!!}
                    </div>
                    <div class="form-group">
                        {!!Form::label('description','Beschrijving')!!}
                        {!!Form::text('description',null,['class'=>'form-control'])!!}
                    </div>

                    <div class="form-group">
                        {!!Form::label('price','Prijs')!!}
                        {!!Form::text('price',null,['class'=>'form-control'])!!}
                    </div>
                    
                    <div class="form-group">
                        {!!Form::label('discountpercentage','Korting')!!}
                        {!!Form::text('discountpercentage',null,['class'=>'form-control'])!!}
                    </div>
                    
                    <div class="form-group">
                        {!!Form::label('shippingcost','Verzendkost')!!}
                        {!!Form::text('shippingcost',null,['class'=>'form-control'])!!}
                    </div>
                    <div class="form-group">
                        {!!Form::label('votes','Stemmen')!!}
                        {!!Form::text('votes',null,['class'=>'form-control'])!!}
                    </div>
                    <div class="form-group">
                        {!!Form::label('totalrating','totaalscore')!!}
                        {!!Form::text('totalrating',null,['class'=>'form-control'])!!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('idcategory','Categorie') !!}
                        <select class="form-control" name="idcategory">
                            @foreach($categories as $category)
                            <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        
                        
                    </div>
                    {!! Form::close() !!}
                    
                </div>
            </div>
            @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
            @endif
        </div>
        <div class="row">
            <div class="col-md-3 col-md-6 sidebar">
                <table class="table table-striped list-group">
                            <tr>
                                <td>Select</td>
                                <td>Naam</td>
                                <td>Beschrijving</td>
                            </tr>
                            @foreach($products as $item)
                            <tr>
                                <td>
                                      <a href="{{ URL::route('product.show',[$item->id]) }}" class="list-group-item">></a>
                                </td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->description}}</td>
                            </tr>
                            @endforeach
                        </table>
            </div>
        </div>
    </div>
</div>
@endsection