@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading"><div class="row">
                        <div class="col pull-left" style="padding:5px;border:2px">
                            <font size="5">Product</font>
                        </div>
                        <div class="col pull-right" style="padding: 5px;border:2px">
                            {!! Form::model($product,array('route'=>['product.update',$product],'method'=>'PUT')) !!}
                            {!! Form::button('Update',['type'=>'submit','class'=>'btn btn-primary']) !!}
                            {{ link_to_route('product.index','Cancel',null,['class'=>'btn btn-default']) }}
                        </div>
                    </div>
                        
                    </div>

                    <div class="panel-body">

                        
                    <div class="form-group">
                        {!!Form::label('name','Naam')!!}
                        {!!Form::text('name',null,['class'=>'form-control'])!!}
                    </div>
                    <div class="form-group">
                        {!!Form::label('description','Beschrijving')!!}
                        {!!Form::text('description',null,['class'=>'form-control'])!!}
                    </div>

                    <div class="form-group">
                        {!!Form::label('price','Prijs')!!}
                        {!!Form::text('price',null,['class'=>'form-control'])!!}
                    </div>
                    
                    <div class="form-group">
                        {!!Form::label('discountpercentage','Korting')!!}
                        {!!Form::text('discountpercentage',null,['class'=>'form-control'])!!}
                    </div>
                    
                    <div class="form-group">
                        {!!Form::label('shippingcost','Verzendkost')!!}
                        {!!Form::text('shippingcost',null,['class'=>'form-control'])!!}
                    </div>
                    <div class="form-group">
                        {!!Form::label('votes','Stemmen')!!}
                        {!!Form::text('votes',null,['class'=>'form-control'])!!}
                    </div>
                    <div class="form-group">
                        {!!Form::label('totalrating','Totaalscore')!!}
                        {!!Form::text('totalrating',null,['class'=>'form-control'])!!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('image','Afbeelding') !!}
                        {!! Form::file('image',['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('idcategory','Land') !!}
                        {!! Form::select('idcategory', $categories, $product->idcategory,['class'=>'form-control']) !!}
                    </div>

                    
                    <div>
                        {!! Form::close() !!}

                    </div>
                </div>
                
            </div>
                           @if($errors->any())
                    <ul class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
        </div>
        <div class="row">
            <div class="col-md-3 col-md-6 sidebar">
                <table class="table table-striped list-group">
                            <tr>
                                <td>Select</td>
                                <td>Naam</td>
                                <td>Beschrijving</td>
                            </tr>
                            @foreach($products as $item)
                            @if($product->id == $item->id)
                            <tr>
                                <td>
                                      <a href="{{ URL::route('product.show',[$item->id]) }}" class="list-group-item active">></a>
                                </td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->description}}</td>
                            </tr>
                            @elseif($product->id != $item->id)
                            <tr>
                                <td>
                                      <a href="{{ URL::route('product.show',[$item->id]) }}" class="list-group-item">></a>
                                </td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->description}}</td>
                            </tr>
                            @endif
                            @endforeach
                        </table>
            </div>
        </div>
    </div>
@endsection