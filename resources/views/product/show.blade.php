@extends('layouts.app') @section('content')
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col pull-left" style="padding:5px;border:2px">
                            <font size="5">Product</font>
                        </div>
                        <div class="col pull-right" style="padding: 5px;border:2px">
                            {!! Form::open(['method' => 'DELETE','route' => ['product.destroy', $product->id]]) !!}
                            {{ link_to_route('product.edit','Updating',[$product->id],['class'=>'btn btn-primary']) }} 
                            {{ link_to_route('product.create','Inserting',null,['class'=>'btn btn-primary']) }}
                            {!! Form::submit('Delete', ['class' => 'btn btn-primary']) !!}
                            {{ link_to_route('product.index','Cancel',null,['class'=>'btn btn-default']) }}
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="form-group">
                        {!!Form::label('name','Naam')!!}<br> {!!Form::text('name',$product->name,['class'=>'form-control','readonly' => true])!!}
                    </div>
                    <div class="form-group">
                        {!!Form::label('description','Beschrijving')!!}<br> {!!Form::text('description',$product->description,['class'=>'form-control','readonly' => true])!!}
                    </div>
                    <div class="form-group">
                        {!!Form::label('price','Prijs')!!}<br> {!!Form::text('price',$product->price,['class'=>'form-control','readonly' => true])!!}
                    </div>
                    <div class="form-group">
                        {!!Form::label('shippingcost','Verzendkosten')!!}<br> {!!Form::text('shippingcost',$product->shippingcost,['class'=>'form-control','readonly' => true])!!}
                    </div>
                    <div class="form-group">
                        {!!Form::label('discountpercentage','Aanbiedingspercentage')!!}<br> {!!Form::text('discountpercentage',$product->discountpercentage,['class'=>'form-control','readonly' => true])!!}
                    </div>
                    <div class="form-group">
                        {!!Form::label('totalrating','Totaal rating')!!}<br> {!!Form::text('totalrating',$product->totalrating,['class'=>'form-control','readonly' => true])!!}
                    </div>
                    <div class="form-group">
                        {!!Form::label('votes','Stemmen')!!}<br> {!!Form::text('votes',$product->votes,['class'=>'form-control','readonly' => true])!!}
                    </div>
                    <div class="form-group">
                        {!!Form::label('idcategory','Categorie')!!}<br> {!!Form::text('idcategory',$product->categories->name,['class'=>'form-control','readonly' => true])!!}
                    </div>
                </div>
            </div>
        </div>
                            {!! Form::close() !!}

        <div class="row">
            <div class="col-md-3 col-md-6 sidebar">
                <table class="table table-striped list-group">
                            <tr>
                                <td>Select</td>
                                <td>Naam</td>
                                <td>Beschrijving</td>
                            </tr>
                            @foreach($products as $item)
                            @if($product->id == $item->id)
                            <tr>
                                <td>
                                      <a href="{{ URL::route('product.show',[$item->id]) }}" class="list-group-item active">></a>
                                </td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->description}}</td>
                            </tr>
                            @elseif($product->id != $item->id)
                            <tr>
                                <td>
                                      <a href="{{ URL::route('product.show',[$item->id]) }}" class="list-group-item">></a>
                                </td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->description}}</td>
                            </tr>
                            @endif
                            @endforeach
                        </table>
            </div>
        </div>
    </div>
</div>
@endsection